/* tl;dr AGPLv3+
   please keep this notice at top
   more info at bottom */

wall = 2;
gap = 2;

battery_depth = 41.3;
battery_width = 98.8;
battery_height = 22.4;
battery_out_to_standard = 5.6;

led_matrix_height = 32.12;
led_matrix_depth = 14.4;
led_matrix_width = 128;

lolin_d1_mini_width = 34.9;
d1_plug_width = 9.3;
d1_depth = 25.8;

holder_inner_width = 235;
holder_inner_depth = battery_depth + 2 * gap;
holder_height = battery_height + led_matrix_depth + gap;

module led_matrix() {
    width = led_matrix_width;
    height = led_matrix_height;
    depth = led_matrix_depth;
    cube([width, height, depth]);

    pin_width = 11.8;
    pin_depth = 13.1;
    pin_height = 2.8;

    pin_x_offset = width;
    pin_y_offset = 9.40;
    pin_z_offset = 5.35;
    translate([pin_x_offset,
               pin_y_offset,
               pin_z_offset]){
        cube([pin_width, pin_depth, pin_height]);}}

module battery(){
    width = battery_width;
    height = battery_height;
    depth = battery_depth;

    out_to_standard = battery_out_to_standard;
    out_to_micro = 5.6;
    top_to_micro = 4.14;

    standard_plug_length = 40;

    plug_to_plug_z = 11.2;

    cube([width, depth, height]);

    translate([width, out_to_standard, top_to_micro]){
        cube([standard_plug_length,
              depth - 2 * out_to_standard,
              height - 2 * top_to_micro]);}}

module lolin_d1_mini(){
    width = lolin_d1_mini_width;
    depth = d1_depth;
    // height is from usb plug to wifi chip
    height = 6;
    cube([width, depth, height]);

    pin_height = 15;
    pin_width = 20.19;
    pin_depth = 2.8;

    pin_x_offset = 5.72;
    pin_y_offset = depth - pin_depth;
    pin_z_offset = height;

    translate([pin_x_offset,
               0,
               pin_z_offset]){
        cube([pin_width, pin_depth, pin_height]);}
    translate([pin_x_offset, pin_y_offset, pin_z_offset]){
        cube([pin_width, pin_depth, pin_height]);}

    micro_plug_length = 31;
    edge_to_plug = 8.6;
    plug_width = d1_plug_width;
    plug_depth = 7.6;
    translate([-micro_plug_length, edge_to_plug, 0]){
        cube([micro_plug_length, plug_width, plug_depth]);}}

module holder(){
    round = 5;
    inner_width = holder_inner_width;
    inner_depth = holder_inner_depth;
    difference(){
        translate([-wall + round/2, -wall + round/2, -wall]){
            linear_extrude(height = holder_height){
                minkowski(){
                    circle(round/2, $fn = 30);
                    square([inner_width + 2 * wall - round,
                            inner_depth + 2 * wall - round]);}}}
        cube([inner_width, inner_depth, inner_depth + 0.1]);
        translate([0, inner_depth / 2, holder_height - 20]){
            rotate([90, 0, -90]){
                translate([0, 0, -0.1]){
                    cylinder(h = wall + 2 * 0.1, r = 10, $fn = 6);}}}
        translate([inner_width, inner_depth / 2, holder_height - 20]){
            rotate([-90, 0, -90]){
                translate([0, 0, -0.1]){
                    cylinder(h = wall + 2 * 0.1, r = 10, $fn = 6);}}}}

    // battery restraint
    translate([gap + battery_width + gap, 0, 0]){
        battery_restraint();}

    // d1 restraint
    d1_restraint_height = holder_height - led_matrix_depth - gap;
    d1_restraint_depth = (inner_depth - d1_plug_width) / 2 - gap;
    translate([inner_width
               - (lolin_d1_mini_width + gap + restraint_width),
               0,
               0]){
        cube([restraint_width,
              d1_restraint_depth,
              d1_restraint_height]);}
    translate([inner_width
               - (lolin_d1_mini_width + gap + restraint_width),
               inner_depth - d1_restraint_depth,
               0]){
        cube([restraint_width,
              d1_restraint_depth,
              d1_restraint_height]);}


    // matrix support
    restraint_width = battery_out_to_standard;
    restraint_height = holder_height - led_matrix_depth - gap;
    translate([gap + battery_width + gap, 0, 0]){
        translate([0, battery_out_to_standard, restraint_height]){
            rotate([0, 90, 0]){
                linear_extrude(height = battery_out_to_standard){
                    polygon([[                      0,
                                                    0],
                             [                      0,
                              battery_out_to_standard],
                             [battery_out_to_standard,
                              0]]);}}}
        translate([0,
                   holder_inner_depth - battery_out_to_standard,
                   restraint_height]){
            rotate([-90, 0, -90]){
                linear_extrude(height = battery_out_to_standard){
                    polygon([[                      0,
                                                    0],
                             [                      0,
                              battery_out_to_standard],
                             [battery_out_to_standard,
                              0]]);}}}}
    led_x_offset = (holder_inner_width - led_matrix_width) / 2;
    led_right_support_depth = 10;
    translate([led_x_offset + led_matrix_width - restraint_width,
               0,
               0]){
        cube([restraint_width,
              led_right_support_depth,
              restraint_height]);}

    translate([led_x_offset + led_matrix_width - restraint_width,
               inner_depth - led_right_support_depth,
               0]){
        cube([restraint_width,
              led_right_support_depth,
              restraint_height]);}}

module battery_restraint(){
    restraint_width = battery_out_to_standard;
    restraint_height = holder_height - led_matrix_depth - gap;
    cube([restraint_width,
          battery_out_to_standard,
          restraint_height]);
    translate([0,
               holder_inner_depth - battery_out_to_standard,
               0]){
        cube([restraint_width,
              battery_out_to_standard,
              restraint_height]);}}


module lid(){
    tolerance = 0.5;

    width = holder_inner_width - tolerance;
    depth = holder_inner_depth - tolerance;

    led_hole_width = led_matrix_width + tolerance;
    led_hole_depth = led_matrix_height + tolerance;

    led_x_offset = (width - led_hole_width) / 2;
    led_y_offset = (depth - led_hole_depth) / 2;

    height = holder_height - wall;

    difference(){
        cube([width, depth, wall]);
        translate([led_x_offset, led_y_offset, -0.1]){
            cube([led_hole_width, led_hole_depth, wall + 0.1 * 2]);}}

    translate([0, 0, -height]){
        cube([wall, depth, height]);}
    translate([0, 0, -20 + 2 * wall]){
        translate([0, depth / 2, 0]){
            rotate([90, 0, -90]){
                translate([0, 0, -0.1]){
                    cylinder(h = wall + 2 * 0.1,
                             r = 10 - tolerance,
                             $fn = 6);}}}}
    translate([width - wall, 0, -height]){
        cube([wall, depth, height]);}
    translate([width + wall, 0, -20 + 2 * wall]){
        translate([0, depth / 2, 0]){
            rotate([90, 0, -90]){
                translate([0, 0, -0.1]){
                    cylinder(h = wall + 2 * 0.1,
                             r = 10 - tolerance,
                             $fn = 6);}}}}}


holder();
%union(){
    translate([0, 0, holder_height - 2 * wall]){
        lid();}
    // translate([(holder_inner_width - led_matrix_width) / 2,
    //             (holder_inner_depth - led_matrix_height)/2,
    //             battery_height + gap]){
    //     led_matrix();}
    // translate([holder_inner_width - (lolin_d1_mini_width + gap),
    //             (holder_inner_depth - d1_depth) / 2,
    //             0]){
    //     lolin_d1_mini();}
    // translate([gap, gap, 0]){
    //     battery();}
}


/*

  https://www.gnu.org/licenses/agpl-3.0.txt

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

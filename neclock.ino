/* tl;dr AGPLv3+
   please keep this notice at top
   more info at bottom */

#include <vector>

struct Alarm {
  int hour;
  int minute;};
std::vector<Alarm> alarms = {
  {18, 54},
  {18, 56}};

#include <NTPClient.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#include <MD_MAX72xx.h>
#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 4

#define CLK_PIN   D5  // or SCK
#define DATA_PIN  D7  // or MOSI
#define CS_PIN    D8  // or SS
MD_MAX72XX mx = MD_MAX72XX(HARDWARE_TYPE, CS_PIN, MAX_DEVICES);

const char* ssid = "trekkie-apprise";
const char* password = "qweruiop";

const long offset_hours = -7;
const long offset_seconds = offset_hours * 60 * 60;

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, offset_seconds);

uint8_t blank[] =
  {
    0b00000000, //0
    0b00000000, //1
    0b00000000, //2
    0b00000000, //3
    0b00000000, //4
    0b00000000, //5
    0b00000000, //6
    0b00000000  //7
  };

uint8_t zero[] =
  {
    0b00000000, //0
    0b00000000, //1
    0b00011000, //2
    0b00101100, //3
    0b00110100, //4
    0b00100100, //5
    0b00100100, //6
    0b00011000  //7
  };

uint8_t one[] =
  {
    0b00000000, //0
    0b00000000, //1
    0b00001000, //2
    0b00011000, //3
    0b00001000, //4
    0b00001000, //5
    0b00001000, //6
    0b00011100  //7
  };

uint8_t two[] =
  {
    0b00000000, //0
    0b00000000, //1
    0b00011000, //2
    0b00100100, //3
    0b00000100, //4
    0b00011000, //5
    0b00100000, //6
    0b00111100  //7
  };

uint8_t three[] =
  {
    0b00000000, //0
    0b00000000, //1
    0b00111100, //2
    0b00001000, //3
    0b00011000, //4
    0b00000100, //5
    0b00100100, //6
    0b00011000  //7
  };

uint8_t four[] =
  {
    0b00000000, //0
    0b00000000, //1
    0b00001100, //2
    0b00010100, //3
    0b00111100, //4
    0b00000100, //5
    0b00000100, //6
    0b00000100  //7
  };

uint8_t five[] =
  {
    0b00000000, //0
    0b00000000, //1
    0b00111100, //2
    0b00100000, //3
    0b00111000, //4
    0b00000100, //5
    0b00100100, //6
    0b00011000  //7
  };

uint8_t six[] =
  {
    0b00000000, //0
    0b00000000, //1
    0b00011000, //2
    0b00100100, //3
    0b00100000, //4
    0b00111000, //5
    0b00100100, //6
    0b00011000  //7
  };

uint8_t seven[] =
  {
    0b00000000, //0
    0b00000000, //1
    0b00111100, //2
    0b00000100, //3
    0b00001000, //4
    0b00010000, //5
    0b00010000, //6
    0b00010000  //7
  };

uint8_t eight[] =
  {
    0b00000000, //0
    0b00000000, //1
    0b00011000, //2
    0b00100100, //3
    0b00011000, //4
    0b00100100, //5
    0b00100100, //6
    0b00011000  //7
  };

uint8_t nine[] =
  {
    0b00000000, //0
    0b00000000, //1
    0b00011000, //2
    0b00100100, //3
    0b00011100, //4
    0b00000100, //5
    0b00000100, //6
    0b00000100  //7
  };

uint8_t *digits[10] =
  {
    zero,
    one,
    two,
    three,
    four,
    five,
    six,
    seven,
    eight,
    nine
  };

void setup() {
  mx.begin();
  mx.clear();
  // default intensity is half max intensity
  // default max intensity is 0xF
  mx.control(MD_MAX72XX::INTENSITY,
             0x0);
  Serial.begin(115200);
  while(!Serial);
  Serial.println("connecting to trekkie");
  WiFi.hostname("asdf");
  WiFi.begin(ssid, password);
  Serial.println("connected!");
  int row = 0;
  int column = 0;
  while(WiFi.status() != WL_CONNECTED){
    Serial.print(".");
    mx.setPoint(row, column, 1);
    row++;
    if (8 <= row){
      row = 0;
      column++;
      if (8 <= column){
        mx.clear();
        column = 0;}}
    delay(1000);}
  Serial.print("\n\nconnected\n\n");
  Serial.print(WiFi.localIP());

  timeClient.begin();}

void separate() {
  mx.setPoint(2, 16, 1);
  mx.setPoint(2, 15, 1);
  mx.setPoint(3, 16, 1);
  mx.setPoint(3, 15, 1);
  mx.setPoint(5, 16, 1);
  mx.setPoint(5, 15, 1);
  mx.setPoint(6, 16, 1);
  mx.setPoint(6, 15, 1);}

void loop() {
  timeClient.update();
  Serial.println(timeClient.getFormattedTime());
  int second = timeClient.getSeconds();
  int minute = timeClient.getMinutes();
  int hour = timeClient.getHours();
  mx.control(MD_MAX72XX::UPDATE, MD_MAX72XX::OFF);
  mx.clear();

  mx.setBuffer(8*1 - 1, 8, digits[minute % 10]);
  mx.setBuffer(8*2 - 1, 8, digits[minute / 10]);
  mx.setBuffer(8*3 - 1, 8, digits[hour % 10]);
  mx.setBuffer(8*4 - 1, 8, digits[hour / 10]);

  mx.transform(MD_MAX72XX::TRC);

  for(const auto &alarm : alarms){
    if(hour == alarm.hour && minute == alarm.minute){
      separate();
      if(second % 2 == 0) {
        mx.transform(MD_MAX72XX::TINV);}
      break;}
    else {
      if(second % 2 == 0) {
        separate();}}}

  mx.control(MD_MAX72XX::UPDATE, MD_MAX72XX::ON);
  delay(1000);}

/*

  https://www.gnu.org/licenses/agpl-3.0.txt

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of
  the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
